<?php
/**
 * Main Class file for CSV Manipulations (Class => csvManipulation)
 *
 * This class file is used to do all sort of CSV Manipulations
 * like Update, edit, delete csv file rows etc. csv file can be
 * changed by sending the path and name of csv file in class contructor.
 * 
 * @category   Class file
 * @package    Address Book 1.0.0
 * @author     Abhinav Verma <abhinav.verma20@gmail.com>
 * @copyright  2014 Abhinav Verma
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    1.0.0
 * @link       https://bitbucket.org/vermaabhinav/address-book.git
 * @since      File available since Release 1.0.0
 */
class csvManipulation {
	
	public $filename = '';
	public $is_csv = FALSE;

	public function __construct($filename = 'data/contacts.csv'){ 
		$this->filename = $filename;
		try{
			if (substr($filename, -3) == 'csv') {
				$this->is_csv = TRUE;
			}else{
				throw new Exception("not a valid csv file !!");
			}
		}catch (Exception $e){
			$error = true;
			echo '<div class="alert alert-danger" role="alert">' . $e->getMessage() ." </div>";
			exit();
		}
	}


	public function read(){
		if ($this->is_csv == TRUE) {
			return $this->readCsv();
		} else {
			return $this->readLines();
		}
	}

	public function save($contents) {
		if ($this->is_csv == TRUE) {
			$this->writeCsv($contents);
		} else {
			$this->writeLines($contents);
		}
	}

    /**
     * Returns array of rows in csv file
     */
	private function readLines(){
		$handle = fopen($this->filename, "r");
		$size = filesize($this->filename);
		$contents = fread($handle, $size);
		fclose($handle);
		return explode("\n", $contents);
	}       
    

    /**
     * Writes each element in $array to a new line in csv file
     */
	private function writeLines($array){
		$handle = fopen($this->filename, "w");
		$item = implode("\n", $array);
		fwrite($handle, $item);
		fclose($handle);
	}

    /**
     * Reads contents of csv file, returns an array
     */
	private function readCsv(){
		$handle = fopen($this->filename, "r"); 
		if (filesize($this->filename) == 0) {
			$contents = [];
		} else {
			while (!feof($handle)) {
				$contents[] = fgetcsv($handle);
			}
			foreach ($contents as $key => $value) {
				if ($value == false) {
					unset($contents[$key]);
				}
			}
		}
		fclose($handle);
		return $contents;
	}

    
    /**
     *   Writes contents of $array to csv file
     */
	private function writeCsv($array){
		// Code to write $addresses_array to file $this->filename
		$handle = fopen($this->filename, "w");
		foreach($array as $fields) {
			fputcsv($handle, $fields);
		}
		fclose($handle);
	}
   
} //class ends here