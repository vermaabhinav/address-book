<?php
/**
 * Main index file for contacts add and delete (file => index.php)
 *
 * In the address book script one person can use this script to 
 * maintain a small address book in CSV file, without having and DB
 * all the contact details are entered at once in CSV file, which give 
 * an ease to user for maintaing a small record of thier contacts.
 * 
 * 
 * @category   index file
 * @package    Address Book 1.0.0
 * @author     Abhinav Verma <abhinav.verma20@gmail.com>
 * @copyright  2014 Abhinav Verma
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    1.0.0
 * @link       https://bitbucket.org/vermaabhinav/address-book.git
 * @since      File available since Release 1.0.0
 */
// class file required to execute the script
require_once('classes/address.inc.php');
// initialize class and contructing
$book = new csvManipulation('data/contacts.csv');
$contacts = $book->read();
$error = false;

class InvaidInputException extends Exception{} //exception handling

try{
	// Error validation
	if (!empty($_POST)) {
		$entry = array();
		$entry['name'] = $_POST['name'];	
		$entry['email'] = $_POST['email'];
		$entry['phone'] = $_POST['phone'];
		$entry['address'] = $_POST['address'];
		$entry['city'] = $_POST['city'];
		// Organizing error messages
		foreach ($entry as $key => $value) {
			if (empty(trim($value))) {
				throw new InvaidInputException("$key can't be empty");
			} else {
				$entries[] = $value;
			}
			if (strlen($value) > 125) {
				throw new InvaidInputException("$key value is greater than 125 characters.");
			}
		}

		
		foreach ($_POST as $key => $value) {
			$_POST[$key] = ($value);
		}

		// If there are no errors, go ahead and save the address book
		if (empty($errors)) {
			array_push($contacts, array_values($entries));
			$book->save($contacts);
		}
		header('Location: index.php');
		exit();
	} 
} catch (InvaidInputException $e) {
	$error = true;
	echo '<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' . $e->getMessage() ." </div>";
}

if (isset($_GET['key'])) {
	//Remove item from $contacts array 
	//Save array to csv file
	foreach ($contacts as $key => $data) {
		if ($_GET['key'] == $key) {
			unset($contacts[$key]);				
		}
		$book->save($contacts);
	}
	header('Location: index.php');
	exit();
}
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Adress Book</title>
		<meta charset="UTF-8">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
		<!-- font-awesome -->
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<style type="text/css">
			.dataTables_filter{
				float: right;
			}
			.dataTables_paginate > ul{
				margin: 0px;
			}
			.dataTables_paginate{
				float: right;
			}
		</style>
	</head>
	<body>
		<h1 align="center">ADDRESS BOOK</h1>
		<div class="container">	
			<table id="contacts" class="table table-striped table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Name</th>
						<th>Email</th> 
						<th>Phone</th>
						<th>Address</th>
						<th>City</th> 
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($contacts as $key => $address){ ?>
					<tr>
						<?php foreach($address as $data){ ?>
						<td><?php echo htmlspecialchars(strip_tags("{$data}")) ?></td>
						<?php } ?>
						<td><?php echo "<a id='remove' name='remove' class='btn btn-danger btn-xs' href='index.php?key=$key'> Remove </a>"; ?></td>
					</tr>
					<?php } ?>
				</tbody>
			</table><br /><br />
			<h2 align="center">Add a new entry to the Address Book</h2>
		    <form align="center" method="POST" action="index.php">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-user"></i> Name</div>
						<input type="text" class="form-control" name="name" id="name" placeholder="Enter name" value="<?php if(isset($_POST['name']) && isset($error)){echo htmlspecialchars($_POST['name'], ENT_QUOTES);}?>" autofocus='autofocus' required />
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-at"></i> Email</div>
						<input type="email" class="form-control" name="email" id="email" placeholder="Enter email" value="<?php if(isset($_POST['email']) && isset($error)){echo htmlspecialchars($_POST['email'], ENT_QUOTES);}?>" required />
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-phone"></i> Phone</div>
						<input type="tel" class="form-control" name="phone" id="phone" placeholder="Enter Phone number" value="<?php if(isset($_POST['phone']) && isset($error)){echo htmlspecialchars($_POST['phone'], ENT_QUOTES);}?>" required />
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-at"></i> Address</div>
						<textarea name="address" rows="4" cols="100%" class="form-control" required><?php if(isset($_POST['address']) && isset($error)){echo htmlspecialchars($_POST['address'], ENT_QUOTES);}?></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-building-o"></i> City</div>
						<input id="city" name="city" placeholder="Enter city" value= "<?if(isset($_POST['city']) && isset($error)){echo htmlspecialchars($_POST['city'], ENT_QUOTES);}?>" type="text" required class="form-control" />
					</div>
				</div>
				<button class="btn btn-success btn-md" type="submit" id="submit" name="submit">Submit</button>
			</form>
		</div>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
		<script src="//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js"></script>
		<script src="//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/bootstrap/3/dataTables.bootstrap.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
			    $('#contacts').dataTable();
			});
		</script>
	</body>
</html>